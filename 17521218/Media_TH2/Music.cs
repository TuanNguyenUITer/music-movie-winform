﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using TagLib;
using static System.Net.WebRequestMethods;

namespace Media_TH2
{
    public partial class Music : Form
    {
       
        public Music()
        {
            InitializeComponent();

            rtbLyric.SelectionAlignment = HorizontalAlignment.Center;
            pnInfo.Visible = false;
        }
      
        
        List<MMusic> listMusic;
        private void btSearch_Click(object sender, EventArgs e)
        {
              var list1= listMusic.FindAll(music => music.tenbh.ToLower().Contains(tbSearch.Text.ToLower()));
            var list2 = listMusic.FindAll(music => music.theloai.ToLower().Contains(tbSearch.Text.ToLower()));
          
        list1=   list1.Concat(list2).ToList();
            if (list1.Count>0)
            {
                listView1.Items.Clear();
                foreach (var baihat in list1)
                {
                    ListViewItem item = new ListViewItem(baihat.tenbh, 0);
                    item.SubItems.Add(baihat.casi);
                    item.SubItems.Add(baihat.theloai);
                    listView1.Items.Add(item);
                }
            }
            else {
                listView1.Items.Clear();
                listView1.Items.Add("Không tìm thấy bài hát nào"); }

        }
        //tải lên file json và trả về danh sách bài hát
        #region load
        List<MMusic> loadJson(string filename)
        {
            List<MMusic> data=new List<MMusic>();
            try
            {
                using (StreamReader r = new StreamReader(filename))
                {
                    string json = r.ReadToEnd();
                     data = JsonConvert.DeserializeObject<List<MMusic>>(json);
                    
                }
            }
            catch 
            {
            }
            return data;
        }
        void loadToListview(string filename)
        {
            listView1.Items.Clear();

            List<MMusic> data = loadJson(filename);
            foreach (var baihat in data)
            {
                ListViewItem item = new ListViewItem(baihat.tenbh, 0);
                item.SubItems.Add(baihat.casi);
                item.SubItems.Add(baihat.theloai);
                listView1.Items.Add(item);
               
            }
        }
        List<MMusic> listRecent;
        void loadToListview(List<MMusic> data)
        {
            listView1.Items.Clear();
            if (data.Count < 1)
            {
                listView1.Items.Add("Không có bài hát nào");
                return;
            }
            foreach (var baihat in data)
            {
                ListViewItem item = new ListViewItem(baihat.tenbh, 0);
                item.SubItems.Add(baihat.casi);
                item.SubItems.Add(baihat.theloai);
                listView1.Items.Add(item);
            }
           
        }
      void  addToRecent(MMusic music)
        {
            listRecent = loadJson(Danhsachvuanghe);
            listRecent.Add(music);
        }
        void addToFile(MMusic music,string filename,List<MMusic> listToAdd)
        {
            listToAdd = loadJson(filename);
           
           listToAdd.Add(music);
            using (System.IO.StreamWriter file = System.IO.File.CreateText(filename))
            {
                JsonSerializer json = new JsonSerializer();
                json.Serialize(file, listToAdd);
            };
        }
        #endregion
        private void Music_Load(object sender, EventArgs e)
        {
            //listview
            listView1.SmallImageList = imageList1;
            listView1.View = View.Details;
            imageList1.Images.Add(Properties.Resources.music_40px);
            listMusic = loadJson(Danhsachtatca);
            loadToListview(listMusic);
            listRecent = loadJson(Danhsachvuanghe);
        }
        public string Danhsachtatca= "Danhsachbaihat.txt";
        public string Danhsachvuanghe = "Danhsachvuanghe.txt";
        public string Danhsachyeuthich = "Danhsachyeuthich.txt";
        private void btThem_Click(object sender, EventArgs e)
        {
            MMusic data =new  MMusic {
                tenbh = tbTenbh.Text,
                path = tbPath.Text,
                casi = tbCasi.Text,
                nhacsi = tbNhacsi.Text,
                loibh = tbLoibh.Text,
                theloai=tbTheloai.Text
            };
            try
            {
                addToFile(data, Danhsachtatca, listMusic);
                listMusic = loadJson(Danhsachtatca);
                loadToListview(listMusic);
                MessageBox.Show("Thêm thành công");
                tbCasi.Text = tbLoibh.Text = tbNhacsi.Text = tbPath.Text = tbTenbh.Text = tbTheloai.Text = "";
            } catch { }
        }
      
        private void btPick_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Title = "Open music";
            dialog.Filter = "Music file (*.mp3) | *.mp3";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
              
                tbPath.Text = dialog.FileName;
                System.IO.FileStream file = System.IO.File.Open(dialog.FileName,FileMode.Open);
                TagLib.File tagFile = TagLib.File.Create(new StreamFileAbstraction(file.Name, file,
                    file));
                tbTenbh.Text = tagFile.Tag.Title;
                if(tagFile.Tag.Performers.Length>0)
                {
                tbCasi.Text = tagFile.Tag.Performers[0];
                }
                if(tagFile.Tag.Artists.Length>0)
                { tbNhacsi.Text = tagFile.Tag.Artists[0]; }
                tbLoibh.Text = tagFile.Tag.Lyrics;
            }
        }


        int indexLVSelected = -1;
        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            indexLVSelected = e.ItemIndex;

            if (e.IsSelected)
            {
                this.btInfo_Click(sender,e);
                switch(focus)
                {
                    case 0:
                        {
                            wdMedia.URL = listMusic[e.ItemIndex].path;

                            if (listMusic[e.ItemIndex].loibh != "")
                            {
                                rtbLyric.Text = listMusic[e.ItemIndex].loibh;

                            }
                            else rtbLyric.Text = "Không có lời bài hát";
                            if (listRecent != null)
                            {
                                listRecent = loadJson(Danhsachvuanghe);
                                if (listRecent.Find(music => music.tenbh == listMusic[listView1.FocusedItem.Index].tenbh) != null)
                                {
                                    int ind=listRecent.IndexOf(listRecent.Find(music => music.tenbh == listMusic[listView1.FocusedItem.Index].tenbh));
                                    listRecent.RemoveAt(ind);
                                }
                                listRecent.Insert(0, listMusic[e.ItemIndex]);
                                if (listRecent.Count > 6)
                                    listRecent.RemoveAt(6);
                                using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachvuanghe))
                                {
                                    JsonSerializer json = new JsonSerializer();
                                    json.Serialize(file, listRecent);
                                };
                            }
                            else
                            {
                                addToFile(listMusic[e.ItemIndex], Danhsachvuanghe, listRecent);
                            }
                            
                            listRecent = loadJson(Danhsachvuanghe);

                        }
                        break;
                    case 1:
                        {
                            wdMedia.URL = listRecent[e.ItemIndex].path;
                            if (listRecent[e.ItemIndex].loibh != "")
                            {
                                rtbLyric.Text = listRecent[e.ItemIndex].loibh;

                            }
                            else rtbLyric.Text = "Không có lời bài hát";
                        }
                        break;
                    case 2:
                        {
                            wdMedia.URL = listFavorite[e.ItemIndex].path;
                            if (listFavorite[e.ItemIndex].loibh != "")
                            {
                                rtbLyric.Text = listFavorite[e.ItemIndex].loibh;

                            }
                            else rtbLyric.Text = "Không có lời bài hát";
                        }
                        break;
                }
              

            }

        }

    
       
        private void btXoa_Click(object sender, EventArgs e)
        {
            try
            {
                wdMedia.URL = "";
                switch(focus)
                {
                    case 0:
                        {
                            listMusic.RemoveAt(listView1.FocusedItem.Index);
                            using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachtatca))
                            {
                                JsonSerializer json = new JsonSerializer();
                                json.Serialize(file, listMusic);
                            };
                        }
                        break;
                    case 1:
                        {
                           
                            listRecent.RemoveAt(listView1.FocusedItem.Index);
                            using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachvuanghe))
                            {
                                JsonSerializer json = new JsonSerializer();
                                json.Serialize(file, listRecent);
                            };
                        }
                        break;
                    case 2:
                        {
                            listFavorite.RemoveAt(listView1.FocusedItem.Index);
                            using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachyeuthich))
                            {
                                JsonSerializer json = new JsonSerializer();
                                json.Serialize(file, listFavorite);
                            };
                        }
                        break;
                }
                 MessageBox.Show("Xóa thành công!");
                listView1.Items.RemoveAt(listView1.FocusedItem.Index);

            }
            catch { }
        }

        private void btRecent_Click(object sender, EventArgs e)
        {
            focus = 1;
            btFavorite.Visible = false;
            label7.Visible = false;

            listRecent = loadJson(Danhsachvuanghe);
            loadToListview(listRecent);

        }
        int focus = 0;
        private void btDanhsachtatca_Click(object sender, EventArgs e)
        {
             focus = 0;
            btFavorite.Visible = true;
            label7.Visible = true;
            listMusic = loadJson(Danhsachtatca);
            loadToListview(listMusic);
        }

        List<MMusic> listFavorite;
        private void btFavorite_Click(object sender, EventArgs e)
        {
            if (listView1.FocusedItem != null)
            {
                if (listFavorite == null)
                    listFavorite = loadJson(Danhsachyeuthich);
                if (listFavorite.Find(music=>music.tenbh==listMusic[listView1.FocusedItem.Index].tenbh)!=null)
                {
                    MessageBox.Show("Bài hát này đã tồn tại trong danh sách yêu thích");
                    return;
                }
                
                
                if (indexLVSelected >= 0)
                {

                    addToFile(listMusic[indexLVSelected], Danhsachyeuthich, listFavorite);
                    MessageBox.Show("Đã thêm vào danh sách yêu thích");

                }
                else MessageBox.Show("Bài hát này đã tồn tại trong bài hát yêu thích");
            }
            else MessageBox.Show("Thêm thất bại");
        }

        private void btListFavorite_Click(object sender, EventArgs e)
        {
            focus = 2;
            btFavorite.Visible = false;
            label7.Visible = false; 
            listFavorite = loadJson(Danhsachyeuthich);
            loadToListview(listFavorite);
        }
        public MMusic musicEdit;
        private void btEdit_Click(object sender, EventArgs e)
        {
            if((focus==0||focus==2)&&listView1.FocusedItem!=null)
            {
               if(focus==0)
                {
                    musicEdit = listMusic[listView1.FocusedItem.Index];

                    DiaLog.EditMusic editForm = new DiaLog.EditMusic();
                    editForm.music = musicEdit;
                    editForm.Show();
                }
            }
        }
        public  void Edit(MMusic music)
        {
            if(focus==0)
            {

                listMusic[listView1.FocusedItem.Index] =
                    music;
                using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachtatca))
                {
                    JsonSerializer json = new JsonSerializer();
                    json.Serialize(file, listMusic);
                };
                loadToListview(Danhsachtatca);

            }
            else if(focus==2)
            {
                listFavorite[listView1.FocusedItem.Index] =
                    music;
                using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachyeuthich))
                {
                    JsonSerializer json = new JsonSerializer();
                    json.Serialize(file, listFavorite);
                };
                loadToListview(Danhsachyeuthich);
            }
            MessageBox.Show("Sửa thành công");
        }

        private void btInfo_Click(object sender, EventArgs e)
        {
            if (indexLVSelected < 0) return;
            if (pnInfo.Visible == false)
                pnInfo.Visible = true;
            else pnInfo.Visible = false;
            MMusic music = new MMusic();
            switch(focus)
            {
                case 0:
                    music = listMusic[indexLVSelected];
                    break;
                case 1:
                    music = listRecent[indexLVSelected];
                    break;
                case 2:
                    music = listFavorite[indexLVSelected];
                    break;
            }
            lbTitle.Text = music.tenbh;
            lbArtist.Text = music.casi;
            lbNhacsi.Text = music.nhacsi;
        }

      
    }

    public class MMusic
    {
        public string tenbh { get; set; }
        public string theloai { get; set; }
        public string path { get; set; }
        public string casi { get; set; }
        public string nhacsi { get; set; }
        public string loibh { get; set; }

    }
}
