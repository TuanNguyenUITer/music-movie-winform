﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media_TH2
{
  public  class Singleton
    {
        static Music music;
        static Movie movie;
        public static Music GetFormMusic
        {
            get
            {
                if (music == null || music.IsDisposed)
                {
                    music = new Music();
                }
                return music;
            }
        }
        public static Movie GetFormMovie
        {
            get
            {
                if (movie == null || movie.IsDisposed)
                {
                    movie = new Movie();
                }
                return movie;
            }
        }
    }
}
