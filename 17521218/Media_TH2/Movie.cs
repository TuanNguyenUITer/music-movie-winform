﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TagLib;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using System.IO;
using Newtonsoft.Json;

namespace Media_TH2
{
    public partial class Movie : Form
    {
        public Movie()
        {
            InitializeComponent();

        }

        List<MMovie> listAll;
        List<MMovie> listRecent;
        public string Danhsachtatca = "DanhsachPhim.txt";
        public string Danhsachvuaxem = "Danhsachvuaxem.txt";
        int focus = 0;
        private void btPick_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Title = "Open music";
            dialog.Filter = "Video file (*.mp4) | *.mp4";

            if (dialog.ShowDialog() == DialogResult.OK)
            {

                //var filemp4 = ShellFile.FromFilePath(dialog.FileName);
                //this.pictureBox2.BackgroundImage = filemp4.Thumbnail.MediumBitmap;
                this.tbPath.Text = dialog.FileName;
                var tmp = dialog.FileName.Split(System.IO.Path.DirectorySeparatorChar);
                this.tbTenp.Text = tmp[tmp.Length - 1];
            }
        }


        private void btThem_Click(object sender, EventArgs e)
        {
            if(tbPath.Text==""||tbTenp.Text=="")
            {
                MessageBox.Show("Chưa nhập đầy đủ thông tin");
                return;

            }
            MMovie data = new MMovie
            {
                tenp = tbTenp.Text,
                path = tbPath.Text,
                dienvien = tbDienvien.Text,
                daodien = tbDaodien.Text,
                theloai = tbTheloai.Text
            };
            try
            {
                addToFile(data, Danhsachtatca, listAll);
                listAll = loadJson(Danhsachtatca);
                loadToListview(listAll);
                MessageBox.Show("Thêm thành công");
                tbDienvien.Text = tbDaodien.Text = tbPath.Text = tbTenp.Text = tbTheloai.Text = "";
            }
            catch { }
        }
      
        private void btSearch_Click(object sender, EventArgs e)
        {
            var list1 = listAll.FindAll(music => music.tenp.ToLower().Contains(tbSearch.Text.ToLower()));
            var list2 = listAll.FindAll(music => music.theloai.ToLower().Contains(tbSearch.Text.ToLower()));

            list1 = list1.Concat(list2).ToList();
            if (list1.Count > 0)
            {
                loadToListview(list1);
            }
            else
            {
                listView1.Items.Clear();
                listView1.Items.Add("Không tìm thấy phim nào");
            }
        }
       
        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                this.btInfo_Click(sender, e);
                switch (focus)
                {
                    case 0:
                        {
                            wdMedia.URL = listAll[e.ItemIndex].path;


                            if (listRecent != null)
                            {
                                listRecent = loadJson(Danhsachvuaxem);
                                if (listRecent.Find(music => music.tenp == listAll[listView1.FocusedItem.Index].tenp) != null)
                                {
                                    int ind = listRecent.IndexOf(listRecent.Find(music => music.tenp == listAll[listView1.FocusedItem.Index].tenp));
                                    listRecent.RemoveAt(ind);
                                }
                                listRecent.Insert(0, listAll[e.ItemIndex]);
                                if (listRecent.Count > 6)
                                    listRecent.RemoveAt(6);
                                using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachvuaxem))
                                {
                                    JsonSerializer json = new JsonSerializer();
                                    json.Serialize(file, listRecent);
                                };
                            }
                            else
                            {
                                addToFile(listAll[e.ItemIndex], Danhsachvuaxem, listRecent);
                            }

                            listRecent = loadJson(Danhsachvuaxem);

                        }
                        break;
                    case 1:
                        {
                            wdMedia.URL = listRecent[e.ItemIndex].path;
                          
                        }
                        break;
                   
                }


            }
        }
        #region addtofile
        void loadToListview(List<MMovie> data)
        {

            listView1.Items.Clear();

            if (data.Count < 1)
            {
                listView1.Items.Add("Không có phim nào");
                return;
            }
            ImageList imglist = new ImageList();
            imglist.ImageSize = new Size(96, 54);
            listView1.SmallImageList = imglist;


            for (int i = 0; i < data.Count; i++)
            {
                var filemp4 = ShellFile.FromFilePath(data[i].path);
                imglist.Images.Add(filemp4.Thumbnail.MediumBitmap);
                ListViewItem item = new ListViewItem(data[i].tenp, i);
                item.SubItems.Add(data[i].dienvien);
                item.SubItems.Add(data[i].theloai);
                listView1.Items.Add(item);
            }


        }
        void addToFile(MMovie movie, string filename, List<MMovie> listToAdd)
        {
            listToAdd = loadJson(filename);

            listToAdd.Add(movie);
            using (System.IO.StreamWriter file = System.IO.File.CreateText(filename))
            {
                JsonSerializer json = new JsonSerializer();
                json.Serialize(file, listToAdd);
            };
        }
        void addToRecent(MMovie movie)
        {
            listRecent = loadJson(Danhsachvuaxem);
            listRecent.Add(movie);
        }
        List<MMovie> loadJson(string filename)
        {
            List<MMovie> data = new List<MMovie>();
            try
            {
                using (StreamReader r = new StreamReader(filename))
                {
                    string json = r.ReadToEnd();
                    data = JsonConvert.DeserializeObject<List<MMovie>>(json);
                }
            }
            catch
            {
            }
            return data;
        }
        #endregion

        private void Movie_Load(object sender, EventArgs e)
        {
            listAll = loadJson(Danhsachtatca);
            listRecent = loadJson(Danhsachvuaxem);
            loadToListview(listAll);
        }
   public  void Edit(MMovie movie)
        {
            if (focus== 0)
            {

                listAll[listView1.FocusedItem.Index] =
                    movie;
                using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachtatca))
                {
                    JsonSerializer json = new JsonSerializer();
                    json.Serialize(file, listAll);
                };
                listAll = loadJson(Danhsachtatca);
                loadToListview(listAll);
                MessageBox.Show("Sửa thành công");

            }
        }
        private void btXoa_Click(object sender, EventArgs e)
        {
            try
            {
                wdMedia.URL = "";
                switch (focus)
                {
                    case 0:
                        {
                            listAll.RemoveAt(listView1.FocusedItem.Index);
                            using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachtatca))
                            {
                                JsonSerializer json = new JsonSerializer();
                                json.Serialize(file, listAll);
                            };
                        }
                        break;
                    case 1:
                        {

                            listRecent.RemoveAt(listView1.FocusedItem.Index);
                            using (System.IO.StreamWriter file = System.IO.File.CreateText(Danhsachvuaxem))
                            {
                                JsonSerializer json = new JsonSerializer();
                                json.Serialize(file, listRecent);
                            };
                        }
                        break;
                   
                }
                 MessageBox.Show("Xóa thành công!");
                listView1.Items.RemoveAt(listView1.FocusedItem.Index);

            }
            catch { }
        }

        private void btDanhsachtatca_Click(object sender, EventArgs e)
        {
            focus = 0;
            loadToListview(listAll);
        }

        private void btRecent_Click(object sender, EventArgs e)
        {
            focus = 1;
            listRecent = loadJson(Danhsachvuaxem);
            loadToListview(listRecent);
        }
        public MMovie movieEdit;
        private void btEdit_Click(object sender, EventArgs e)
        {
            if ((focus == 0) && listView1.FocusedItem != null)
            {
                if (focus == 0)
                {
                    movieEdit = listAll[listView1.FocusedItem.Index];

                    DiaLog.EditMovie editForm = new DiaLog.EditMovie();
                    editForm.movie = movieEdit;
                    editForm.Show();
                }
            }
        }

        private void btInfo_Click(object sender, EventArgs e)
        {
            if (listView1.FocusedItem == null)
                return;
           
            if (focus==0)
            {
                MMovie movie = listAll[listView1.FocusedItem.Index];
                var filemp4 = ShellFile.FromFilePath(movie.path);
                pictureBox1.Image = filemp4.Thumbnail.LargeBitmap;
                lbTenphim.Text = movie.tenp;
                lbDv.Text = movie.dienvien;
                //pictureBox1.Size = new Size(96, 54);
            }
            else if(focus==1)
            {
                MMovie movie = listRecent[listView1.FocusedItem.Index];
                var filemp4 = ShellFile.FromFilePath(movie.path);
                pictureBox1.Image = filemp4.Thumbnail.LargeBitmap;
                lbTenphim.Text = movie.tenp;
                lbDv.Text = movie.dienvien;
                //pictureBox1.Size = new Size(96, 54);
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
    public class MMovie
    {
        public string tenp { get; set; }
        public string theloai { get; set; }
        public string path { get; set; }
        public string dienvien { get; set; }
        public string daodien { get; set; }

    }
}
