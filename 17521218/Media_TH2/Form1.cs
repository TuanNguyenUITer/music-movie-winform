﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Media_TH2
{
    public partial class Form1 : Form
    {
        public Form1()        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Icon = Properties.Resources.mediaIcon;

        }

        private void btExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btMusic_Click(object sender, EventArgs e)
        {
            Singleton.GetFormMusic.Show();
           
        }

        private void btMovie_Click(object sender, EventArgs e)
        {
            Singleton.GetFormMovie.Show();

        }
    }
}
