﻿namespace Media_TH2
{
    partial class Music
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Music));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnInfo = new System.Windows.Forms.Panel();
            this.lbNhacsi = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbArtist = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbTitle = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btInfo = new System.Windows.Forms.Button();
            this.btListFavorite = new System.Windows.Forms.Button();
            this.btRecent = new System.Windows.Forms.Button();
            this.btDanhsachtatca = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.btEdit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btXoa = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btFavorite = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tbTheloai = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btPick = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btThem = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbLoibh = new System.Windows.Forms.RichTextBox();
            this.tbNhacsi = new System.Windows.Forms.TextBox();
            this.tbCasi = new System.Windows.Forms.TextBox();
            this.tbTenbh = new System.Windows.Forms.TextBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Ten = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Casi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Theloai = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.btSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rtbLyric = new System.Windows.Forms.RichTextBox();
            this.wdMedia = new AxWMPLib.AxWindowsMediaPlayer();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btLogo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnInfo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wdMedia)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(-1, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 107);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.LightSalmon;
            this.panel1.Controls.Add(this.pnInfo);
            this.panel1.Controls.Add(this.btInfo);
            this.panel1.Controls.Add(this.btListFavorite);
            this.panel1.Controls.Add(this.btRecent);
            this.panel1.Controls.Add(this.btDanhsachtatca);
            this.panel1.Location = new System.Drawing.Point(-1, 112);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(177, 569);
            this.panel1.TabIndex = 1;
            // 
            // pnInfo
            // 
            this.pnInfo.Controls.Add(this.lbNhacsi);
            this.pnInfo.Controls.Add(this.label13);
            this.pnInfo.Controls.Add(this.lbArtist);
            this.pnInfo.Controls.Add(this.label12);
            this.pnInfo.Controls.Add(this.lbTitle);
            this.pnInfo.Controls.Add(this.label10);
            this.pnInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnInfo.Location = new System.Drawing.Point(0, 216);
            this.pnInfo.Name = "pnInfo";
            this.pnInfo.Size = new System.Drawing.Size(177, 353);
            this.pnInfo.TabIndex = 4;
            // 
            // lbNhacsi
            // 
            this.lbNhacsi.AutoSize = true;
            this.lbNhacsi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhacsi.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbNhacsi.Location = new System.Drawing.Point(13, 216);
            this.lbNhacsi.Name = "lbNhacsi";
            this.lbNhacsi.Size = new System.Drawing.Size(0, 25);
            this.lbNhacsi.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 185);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 17);
            this.label13.TabIndex = 4;
            this.label13.Text = "Sáng tác:";
            // 
            // lbArtist
            // 
            this.lbArtist.AutoSize = true;
            this.lbArtist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbArtist.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbArtist.Location = new System.Drawing.Point(13, 150);
            this.lbArtist.Name = "lbArtist";
            this.lbArtist.Size = new System.Drawing.Size(0, 25);
            this.lbArtist.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 119);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 17);
            this.label12.TabIndex = 2;
            this.label12.Text = "Thể hiện:";
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbTitle.Location = new System.Drawing.Point(11, 63);
            this.lbTitle.MaximumSize = new System.Drawing.Size(150, 80);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(0, 25);
            this.lbTitle.TabIndex = 1;
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Bài hát:";
            // 
            // btInfo
            // 
            this.btInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btInfo.Image = global::Media_TH2.Properties.Resources.arow;
            this.btInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btInfo.Location = new System.Drawing.Point(0, 162);
            this.btInfo.Name = "btInfo";
            this.btInfo.Size = new System.Drawing.Size(177, 54);
            this.btInfo.TabIndex = 3;
            this.btInfo.Text = "Thông tin BH";
            this.btInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btInfo.UseVisualStyleBackColor = true;
            this.btInfo.Click += new System.EventHandler(this.btInfo_Click);
            // 
            // btListFavorite
            // 
            this.btListFavorite.Dock = System.Windows.Forms.DockStyle.Top;
            this.btListFavorite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btListFavorite.Image = global::Media_TH2.Properties.Resources.arow;
            this.btListFavorite.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btListFavorite.Location = new System.Drawing.Point(0, 108);
            this.btListFavorite.Name = "btListFavorite";
            this.btListFavorite.Size = new System.Drawing.Size(177, 54);
            this.btListFavorite.TabIndex = 2;
            this.btListFavorite.Text = "Yêu thích";
            this.btListFavorite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btListFavorite.UseVisualStyleBackColor = true;
            this.btListFavorite.Click += new System.EventHandler(this.btListFavorite_Click);
            // 
            // btRecent
            // 
            this.btRecent.Dock = System.Windows.Forms.DockStyle.Top;
            this.btRecent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRecent.Image = global::Media_TH2.Properties.Resources.arow;
            this.btRecent.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btRecent.Location = new System.Drawing.Point(0, 54);
            this.btRecent.Name = "btRecent";
            this.btRecent.Size = new System.Drawing.Size(177, 54);
            this.btRecent.TabIndex = 1;
            this.btRecent.Text = "Vừa nghe";
            this.btRecent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btRecent.UseVisualStyleBackColor = true;
            this.btRecent.Click += new System.EventHandler(this.btRecent_Click);
            // 
            // btDanhsachtatca
            // 
            this.btDanhsachtatca.Dock = System.Windows.Forms.DockStyle.Top;
            this.btDanhsachtatca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDanhsachtatca.Image = global::Media_TH2.Properties.Resources.arow;
            this.btDanhsachtatca.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btDanhsachtatca.Location = new System.Drawing.Point(0, 0);
            this.btDanhsachtatca.Name = "btDanhsachtatca";
            this.btDanhsachtatca.Size = new System.Drawing.Size(177, 54);
            this.btDanhsachtatca.TabIndex = 0;
            this.btDanhsachtatca.Text = "Tất cả bài hát";
            this.btDanhsachtatca.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btDanhsachtatca.UseVisualStyleBackColor = true;
            this.btDanhsachtatca.Click += new System.EventHandler(this.btDanhsachtatca_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.listView1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(182, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(425, 680);
            this.panel2.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.btEdit);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.btXoa);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.btFavorite);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 446);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(425, 66);
            this.panel6.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(281, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 17);
            this.label9.TabIndex = 5;
            this.label9.Text = "Chỉnh sửa";
            // 
            // btEdit
            // 
            this.btEdit.BackgroundImage = global::Media_TH2.Properties.Resources.edit_property_40px;
            this.btEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEdit.Location = new System.Drawing.Point(293, 5);
            this.btEdit.Name = "btEdit";
            this.btEdit.Size = new System.Drawing.Size(50, 38);
            this.btEdit.TabIndex = 4;
            this.btEdit.UseVisualStyleBackColor = true;
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(194, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Xóa";
            // 
            // btXoa
            // 
            this.btXoa.BackgroundImage = global::Media_TH2.Properties.Resources.trash_40px;
            this.btXoa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btXoa.Location = new System.Drawing.Point(184, 5);
            this.btXoa.Name = "btXoa";
            this.btXoa.Size = new System.Drawing.Size(45, 38);
            this.btXoa.TabIndex = 2;
            this.btXoa.UseVisualStyleBackColor = true;
            this.btXoa.Click += new System.EventHandler(this.btXoa_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(64, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Yêu thích";
            // 
            // btFavorite
            // 
            this.btFavorite.BackgroundImage = global::Media_TH2.Properties.Resources.heart_suit_48px;
            this.btFavorite.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btFavorite.Location = new System.Drawing.Point(67, 5);
            this.btFavorite.Name = "btFavorite";
            this.btFavorite.Size = new System.Drawing.Size(47, 38);
            this.btFavorite.TabIndex = 0;
            this.btFavorite.UseVisualStyleBackColor = true;
            this.btFavorite.Click += new System.EventHandler(this.btFavorite_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Controls.Add(this.tbTheloai);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.btPick);
            this.panel5.Controls.Add(this.tbPath);
            this.panel5.Controls.Add(this.btThem);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.tbLoibh);
            this.panel5.Controls.Add(this.tbNhacsi);
            this.panel5.Controls.Add(this.tbCasi);
            this.panel5.Controls.Add(this.tbTenbh);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 512);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(425, 168);
            this.panel5.TabIndex = 2;
            // 
            // tbTheloai
            // 
            this.tbTheloai.FormattingEnabled = true;
            this.tbTheloai.Items.AddRange(new object[] {
            "Nhạc trẻ",
            "Nhạc trữ tình",
            "Nhạc Rap",
            "Nhạc Bolero",
            "Nhạc thiếu nhi",
            "Nhạc Vàng",
            "Nhạc cách mạng",
            "Nhạc Rock",
            "Nhạc nước ngoài"});
            this.tbTheloai.Location = new System.Drawing.Point(21, 82);
            this.tbTheloai.Name = "tbTheloai";
            this.tbTheloai.Size = new System.Drawing.Size(121, 24);
            this.tbTheloai.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(309, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "File:*";
            // 
            // btPick
            // 
            this.btPick.BackgroundImage = global::Media_TH2.Properties.Resources.pick;
            this.btPick.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPick.Location = new System.Drawing.Point(376, 32);
            this.btPick.Name = "btPick";
            this.btPick.Size = new System.Drawing.Size(32, 32);
            this.btPick.TabIndex = 13;
            this.btPick.UseVisualStyleBackColor = true;
            this.btPick.Click += new System.EventHandler(this.btPick_Click);
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(309, 37);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(61, 22);
            this.tbPath.TabIndex = 12;
            // 
            // btThem
            // 
            this.btThem.BackgroundImage = global::Media_TH2.Properties.Resources.add_96px;
            this.btThem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btThem.Location = new System.Drawing.Point(345, 90);
            this.btThem.Name = "btThem";
            this.btThem.Size = new System.Drawing.Size(63, 51);
            this.btThem.TabIndex = 11;
            this.btThem.UseVisualStyleBackColor = true;
            this.btThem.Click += new System.EventHandler(this.btThem_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(159, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Lời bài hát:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(159, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Nhạc sĩ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ca sĩ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Thể loại";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tên bài hát:*";
            // 
            // tbLoibh
            // 
            this.tbLoibh.Location = new System.Drawing.Point(161, 82);
            this.tbLoibh.Name = "tbLoibh";
            this.tbLoibh.Size = new System.Drawing.Size(132, 67);
            this.tbLoibh.TabIndex = 5;
            this.tbLoibh.Text = "";
            // 
            // tbNhacsi
            // 
            this.tbNhacsi.Location = new System.Drawing.Point(161, 37);
            this.tbNhacsi.Name = "tbNhacsi";
            this.tbNhacsi.Size = new System.Drawing.Size(132, 22);
            this.tbNhacsi.TabIndex = 3;
            // 
            // tbCasi
            // 
            this.tbCasi.Location = new System.Drawing.Point(18, 127);
            this.tbCasi.Name = "tbCasi";
            this.tbCasi.Size = new System.Drawing.Size(123, 22);
            this.tbCasi.TabIndex = 2;
            // 
            // tbTenbh
            // 
            this.tbTenbh.Location = new System.Drawing.Point(18, 37);
            this.tbTenbh.Name = "tbTenbh";
            this.tbTenbh.Size = new System.Drawing.Size(123, 22);
            this.tbTenbh.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Ten,
            this.Casi,
            this.Theloai});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 107);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(425, 338);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listView1_ItemSelectionChanged);
            // 
            // Ten
            // 
            this.Ten.Text = "Tên bài hát";
            this.Ten.Width = 160;
            // 
            // Casi
            // 
            this.Casi.Text = "Ca sĩ";
            this.Casi.Width = 70;
            // 
            // Theloai
            // 
            this.Theloai.Text = "Thể loại";
            this.Theloai.Width = 80;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.btSearch);
            this.panel3.Controls.Add(this.tbSearch);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(425, 107);
            this.panel3.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(279, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "Tìm kiếm";
            // 
            // btSearch
            // 
            this.btSearch.BackgroundImage = global::Media_TH2.Properties.Resources.search_64px;
            this.btSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btSearch.Location = new System.Drawing.Point(284, 34);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(41, 33);
            this.btSearch.TabIndex = 1;
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(27, 39);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(251, 22);
            this.tbSearch.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel4.Controls.Add(this.rtbLyric);
            this.panel4.Controls.Add(this.wdMedia);
            this.panel4.Location = new System.Drawing.Point(613, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(600, 686);
            this.panel4.TabIndex = 3;
            // 
            // rtbLyric
            // 
            this.rtbLyric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLyric.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbLyric.Location = new System.Drawing.Point(0, 404);
            this.rtbLyric.Name = "rtbLyric";
            this.rtbLyric.Size = new System.Drawing.Size(600, 282);
            this.rtbLyric.TabIndex = 1;
            this.rtbLyric.Text = "";
            // 
            // wdMedia
            // 
            this.wdMedia.Dock = System.Windows.Forms.DockStyle.Top;
            this.wdMedia.Enabled = true;
            this.wdMedia.Location = new System.Drawing.Point(0, 0);
            this.wdMedia.Name = "wdMedia";
            this.wdMedia.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wdMedia.OcxState")));
            this.wdMedia.Size = new System.Drawing.Size(600, 404);
            this.wdMedia.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(32, 32);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // btLogo
            // 
            this.btLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btLogo.BackgroundImage")));
            this.btLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btLogo.Location = new System.Drawing.Point(-1, 1);
            this.btLogo.Name = "btLogo";
            this.btLogo.Size = new System.Drawing.Size(177, 105);
            this.btLogo.TabIndex = 4;
            this.btLogo.UseVisualStyleBackColor = true;
            // 
            // Music
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 686);
            this.Controls.Add(this.btLogo);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Music";
            this.Text = "Music";
            this.Load += new System.EventHandler(this.Music_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.pnInfo.ResumeLayout(false);
            this.pnInfo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.wdMedia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btDanhsachtatca;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Button btInfo;
        private System.Windows.Forms.Button btListFavorite;
        private System.Windows.Forms.Button btRecent;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListView listView1;
        private AxWMPLib.AxWindowsMediaPlayer wdMedia;
        private System.Windows.Forms.RichTextBox tbLoibh;
        private System.Windows.Forms.TextBox tbNhacsi;
        private System.Windows.Forms.TextBox tbCasi;
        private System.Windows.Forms.TextBox tbTenbh;
        private System.Windows.Forms.RichTextBox rtbLyric;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btThem;
        private System.Windows.Forms.Button btPick;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ColumnHeader Ten;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btFavorite;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btEdit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btXoa;
        private System.Windows.Forms.ComboBox tbTheloai;
        private System.Windows.Forms.ColumnHeader Casi;
        private System.Windows.Forms.ColumnHeader Theloai;
        private System.Windows.Forms.Button btLogo;
        private System.Windows.Forms.Panel pnInfo;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbArtist;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbNhacsi;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
    }
}