﻿namespace Media_TH2.DiaLog
{
    partial class EditMusic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel5 = new System.Windows.Forms.Panel();
            this.tbTheloai = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btPick = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btEdit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbLoibh = new System.Windows.Forms.RichTextBox();
            this.tbNhacsi = new System.Windows.Forms.TextBox();
            this.tbCasi = new System.Windows.Forms.TextBox();
            this.tbTenbh = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.tbTheloai);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.btPick);
            this.panel5.Controls.Add(this.tbPath);
            this.panel5.Controls.Add(this.btEdit);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.tbLoibh);
            this.panel5.Controls.Add(this.tbNhacsi);
            this.panel5.Controls.Add(this.tbCasi);
            this.panel5.Controls.Add(this.tbTenbh);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(504, 284);
            this.panel5.TabIndex = 3;
            // 
            // tbTheloai
            // 
            this.tbTheloai.FormattingEnabled = true;
            this.tbTheloai.Items.AddRange(new object[] {
            "Nhạc trẻ",
            "Nhạc trữ tình",
            "Nhạc Rap",
            "Nhạc Bolero",
            "Nhạc thiếu nhi",
            "Nhạc Vàng",
            "Nhạc cách mạng",
            "Nhạc Rock",
            "Nhạc nước ngoài"});
            this.tbTheloai.Location = new System.Drawing.Point(18, 82);
            this.tbTheloai.Name = "tbTheloai";
            this.tbTheloai.Size = new System.Drawing.Size(156, 24);
            this.tbTheloai.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(247, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "File:";
            // 
            // btPick
            // 
            this.btPick.BackgroundImage = global::Media_TH2.Properties.Resources.pick;
            this.btPick.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPick.Location = new System.Drawing.Point(347, 74);
            this.btPick.Name = "btPick";
            this.btPick.Size = new System.Drawing.Size(34, 32);
            this.btPick.TabIndex = 13;
            this.btPick.UseVisualStyleBackColor = true;
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(250, 84);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(91, 22);
            this.tbPath.TabIndex = 12;
            // 
            // btEdit
            // 
            this.btEdit.BackgroundImage = global::Media_TH2.Properties.Resources.edit_property_40px;
            this.btEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEdit.Location = new System.Drawing.Point(402, 90);
            this.btEdit.Name = "btEdit";
            this.btEdit.Size = new System.Drawing.Size(63, 51);
            this.btEdit.TabIndex = 11;
            this.btEdit.UseVisualStyleBackColor = true;
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Lời bài hát:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(247, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Nhạc sĩ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ca sĩ:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Thể loại";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tên bài hát:";
            // 
            // tbLoibh
            // 
            this.tbLoibh.Location = new System.Drawing.Point(18, 172);
            this.tbLoibh.Name = "tbLoibh";
            this.tbLoibh.Size = new System.Drawing.Size(363, 109);
            this.tbLoibh.TabIndex = 5;
            this.tbLoibh.Text = "";
            // 
            // tbNhacsi
            // 
            this.tbNhacsi.Location = new System.Drawing.Point(249, 37);
            this.tbNhacsi.Name = "tbNhacsi";
            this.tbNhacsi.Size = new System.Drawing.Size(132, 22);
            this.tbNhacsi.TabIndex = 3;
            // 
            // tbCasi
            // 
            this.tbCasi.Location = new System.Drawing.Point(18, 127);
            this.tbCasi.Name = "tbCasi";
            this.tbCasi.Size = new System.Drawing.Size(156, 22);
            this.tbCasi.TabIndex = 2;
            // 
            // tbTenbh
            // 
            this.tbTenbh.Location = new System.Drawing.Point(18, 37);
            this.tbTenbh.Name = "tbTenbh";
            this.tbTenbh.Size = new System.Drawing.Size(156, 22);
            this.tbTenbh.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(415, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Sửa";
            // 
            // EditMusic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 284);
            this.Controls.Add(this.panel5);
            this.Name = "EditMusic";
            this.Text = "EditMusic";
            this.Load += new System.EventHandler(this.EditMusic_Load);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox tbTheloai;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btPick;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Button btEdit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox tbLoibh;
        private System.Windows.Forms.TextBox tbNhacsi;
        private System.Windows.Forms.TextBox tbCasi;
        private System.Windows.Forms.TextBox tbTenbh;
        private System.Windows.Forms.Label label7;
    }
}