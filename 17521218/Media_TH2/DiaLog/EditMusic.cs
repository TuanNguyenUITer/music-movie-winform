﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Media_TH2.DiaLog
{
    public partial class EditMusic : Form
    {
        public EditMusic()
        {
            InitializeComponent();
        }
        public MMusic music;
        private void EditMusic_Load(object sender, EventArgs e)
        {
            tbCasi.Text = music.casi;
            tbLoibh.Text = music.loibh;
            tbPath.Text = music.path;
            tbTenbh.Text = music.tenbh;
            tbTheloai.Text = music.theloai;
            tbNhacsi.Text = music.nhacsi;
        }

       

        private void btEdit_Click(object sender, EventArgs e)
        {
           

            MMusic musicEdit = new MMusic
            {
                tenbh = tbTenbh.Text,
                casi = tbCasi.Text,
                loibh = tbLoibh.Text,
                path = tbPath.Text,
                theloai = tbTheloai.Text,
                nhacsi = tbNhacsi.Text,



            };

            Singleton.GetFormMusic.Edit(musicEdit);
            Singleton.GetFormMusic.Show();
            this.Dispose();
        }
    }
}
