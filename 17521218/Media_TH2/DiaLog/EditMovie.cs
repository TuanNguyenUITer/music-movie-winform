﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Media_TH2.DiaLog
{
    public partial class EditMovie : Form
    {
        public EditMovie()
        {
            InitializeComponent();
        }
        public MMovie movie;
        private void EditMovie_Load(object sender, EventArgs e)
        {
            tbCasi.Text = movie.dienvien;
            tbPath.Text = movie.path;
            tbTenbh.Text = movie.tenp;
            tbTheloai.Text = movie.theloai;
            tbNhacsi.Text = movie.daodien;
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            MMovie movieEdit = new MMovie
            {
                tenp = tbTenbh.Text,
                dienvien = tbCasi.Text,
                path = tbPath.Text,
                theloai = tbTheloai.Text,
                daodien = tbNhacsi.Text,



            };

            Singleton.GetFormMovie.Edit(movieEdit);
            Singleton.GetFormMovie
                .Show();
            this.Dispose();
        }
    }
}
