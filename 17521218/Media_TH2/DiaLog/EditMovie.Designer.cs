﻿namespace Media_TH2.DiaLog
{
    partial class EditMovie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btPick = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btEdit = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNhacsi = new System.Windows.Forms.TextBox();
            this.tbCasi = new System.Windows.Forms.TextBox();
            this.tbTenbh = new System.Windows.Forms.TextBox();
            this.tbTheloai = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(283, 213);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Sửa";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 17);
            this.label6.TabIndex = 29;
            this.label6.Text = "File:";
            // 
            // btPick
            // 
            this.btPick.BackgroundImage = global::Media_TH2.Properties.Resources.pick;
            this.btPick.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPick.Location = new System.Drawing.Point(367, 107);
            this.btPick.Name = "btPick";
            this.btPick.Size = new System.Drawing.Size(34, 32);
            this.btPick.TabIndex = 28;
            this.btPick.UseVisualStyleBackColor = true;
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(270, 117);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(91, 22);
            this.tbPath.TabIndex = 27;
            // 
            // btEdit
            // 
            this.btEdit.BackgroundImage = global::Media_TH2.Properties.Resources.edit_property_40px;
            this.btEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEdit.Location = new System.Drawing.Point(270, 151);
            this.btEdit.Name = "btEdit";
            this.btEdit.Size = new System.Drawing.Size(63, 51);
            this.btEdit.TabIndex = 26;
            this.btEdit.UseVisualStyleBackColor = true;
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(267, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 24;
            this.label4.Text = "Đạo diễn:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 23;
            this.label3.Text = "Diễn viên:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 22;
            this.label2.Text = "Thể loại";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "Tên phim:";
            // 
            // tbNhacsi
            // 
            this.tbNhacsi.Location = new System.Drawing.Point(269, 70);
            this.tbNhacsi.Name = "tbNhacsi";
            this.tbNhacsi.Size = new System.Drawing.Size(132, 22);
            this.tbNhacsi.TabIndex = 19;
            // 
            // tbCasi
            // 
            this.tbCasi.Location = new System.Drawing.Point(38, 160);
            this.tbCasi.Name = "tbCasi";
            this.tbCasi.Size = new System.Drawing.Size(156, 22);
            this.tbCasi.TabIndex = 18;
            // 
            // tbTenbh
            // 
            this.tbTenbh.Location = new System.Drawing.Point(38, 70);
            this.tbTenbh.Name = "tbTenbh";
            this.tbTenbh.Size = new System.Drawing.Size(156, 22);
            this.tbTenbh.TabIndex = 17;
            // 
            // tbTheloai
            // 
            this.tbTheloai.FormattingEnabled = true;
            this.tbTheloai.Items.AddRange(new object[] {
            "Phim hài",
            "Phim tình cảm",
            "Phim trẻ em",
            "Phim Ngôn tình",
            "Phim Tàu khựa",
            "Phim Thời sự",
            "Phim Ngoại quốc"});
            this.tbTheloai.Location = new System.Drawing.Point(38, 113);
            this.tbTheloai.Name = "tbTheloai";
            this.tbTheloai.Size = new System.Drawing.Size(156, 24);
            this.tbTheloai.TabIndex = 32;
            // 
            // EditMovie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 239);
            this.Controls.Add(this.tbTheloai);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btPick);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.btEdit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbNhacsi);
            this.Controls.Add(this.tbCasi);
            this.Controls.Add(this.tbTenbh);
            this.Name = "EditMovie";
            this.Text = "EditMovie";
            this.Load += new System.EventHandler(this.EditMovie_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btPick;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Button btEdit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNhacsi;
        private System.Windows.Forms.TextBox tbCasi;
        private System.Windows.Forms.TextBox tbTenbh;
        private System.Windows.Forms.ComboBox tbTheloai;
    }
}