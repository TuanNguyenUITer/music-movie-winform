﻿namespace Media_TH2
{
    partial class Movie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Movie));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btInfo = new System.Windows.Forms.Button();
            this.btRecent = new System.Windows.Forms.Button();
            this.btDanhsachtatca = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.btEdit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btXoa = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.tbTheloai = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btPick = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btThem = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDaodien = new System.Windows.Forms.TextBox();
            this.tbDienvien = new System.Windows.Forms.TextBox();
            this.tbTenp = new System.Windows.Forms.TextBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Ten = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Casi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Theloai = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel4 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.btSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pnInfo = new System.Windows.Forms.Panel();
            this.lbNhacsi = new System.Windows.Forms.Label();
            this.lbArtist = new System.Windows.Forms.Label();
            this.lbDv = new System.Windows.Forms.Label();
            this.lbTenp = new System.Windows.Forms.Label();
            this.wdMedia = new AxWMPLib.AxWindowsMediaPlayer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbTenphim = new System.Windows.Forms.Label();
            this.lbDienvien = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.pnInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wdMedia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Pink;
            this.panel1.Controls.Add(this.btInfo);
            this.panel1.Controls.Add(this.btRecent);
            this.panel1.Controls.Add(this.btDanhsachtatca);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Location = new System.Drawing.Point(0, 105);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 603);
            this.panel1.TabIndex = 3;
            // 
            // btInfo
            // 
            this.btInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btInfo.Image = global::Media_TH2.Properties.Resources.arow;
            this.btInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btInfo.Location = new System.Drawing.Point(0, 108);
            this.btInfo.Name = "btInfo";
            this.btInfo.Size = new System.Drawing.Size(186, 54);
            this.btInfo.TabIndex = 3;
            this.btInfo.Text = "Thông tin phim";
            this.btInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btInfo.UseVisualStyleBackColor = true;
            this.btInfo.Click += new System.EventHandler(this.btInfo_Click);
            // 
            // btRecent
            // 
            this.btRecent.Dock = System.Windows.Forms.DockStyle.Top;
            this.btRecent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRecent.Image = global::Media_TH2.Properties.Resources.arow;
            this.btRecent.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btRecent.Location = new System.Drawing.Point(0, 54);
            this.btRecent.Name = "btRecent";
            this.btRecent.Size = new System.Drawing.Size(186, 54);
            this.btRecent.TabIndex = 1;
            this.btRecent.Text = "Vừa xem";
            this.btRecent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btRecent.UseVisualStyleBackColor = true;
            this.btRecent.Click += new System.EventHandler(this.btRecent_Click);
            // 
            // btDanhsachtatca
            // 
            this.btDanhsachtatca.Dock = System.Windows.Forms.DockStyle.Top;
            this.btDanhsachtatca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDanhsachtatca.Image = global::Media_TH2.Properties.Resources.arow;
            this.btDanhsachtatca.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btDanhsachtatca.Location = new System.Drawing.Point(0, 0);
            this.btDanhsachtatca.Name = "btDanhsachtatca";
            this.btDanhsachtatca.Size = new System.Drawing.Size(186, 54);
            this.btDanhsachtatca.TabIndex = 0;
            this.btDanhsachtatca.Text = "Tất cả video";
            this.btDanhsachtatca.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btDanhsachtatca.UseVisualStyleBackColor = true;
            this.btDanhsachtatca.Click += new System.EventHandler(this.btDanhsachtatca_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(186, 708);
            this.panel2.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::Media_TH2.Properties.Resources.film04;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(31, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 92);
            this.button1.TabIndex = 5;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Pink;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.ImageLocation = "";
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.pictureBox2.Size = new System.Drawing.Size(186, 105);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.listView1);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel3.Location = new System.Drawing.Point(186, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(425, 708);
            this.panel3.TabIndex = 6;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.btEdit);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.btXoa);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 474);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(425, 66);
            this.panel6.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(281, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 17);
            this.label9.TabIndex = 5;
            this.label9.Text = "Chỉnh sửa";
            // 
            // btEdit
            // 
            this.btEdit.BackgroundImage = global::Media_TH2.Properties.Resources.edit_property_40px;
            this.btEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEdit.Location = new System.Drawing.Point(293, 5);
            this.btEdit.Name = "btEdit";
            this.btEdit.Size = new System.Drawing.Size(50, 38);
            this.btEdit.TabIndex = 4;
            this.btEdit.UseVisualStyleBackColor = true;
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(117, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Xóa";
            // 
            // btXoa
            // 
            this.btXoa.BackgroundImage = global::Media_TH2.Properties.Resources.trash_40px;
            this.btXoa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btXoa.Location = new System.Drawing.Point(107, 5);
            this.btXoa.Name = "btXoa";
            this.btXoa.Size = new System.Drawing.Size(45, 38);
            this.btXoa.TabIndex = 2;
            this.btXoa.UseVisualStyleBackColor = true;
            this.btXoa.Click += new System.EventHandler(this.btXoa_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.tbTheloai);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.btPick);
            this.panel5.Controls.Add(this.tbPath);
            this.panel5.Controls.Add(this.btThem);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.tbDaodien);
            this.panel5.Controls.Add(this.tbDienvien);
            this.panel5.Controls.Add(this.tbTenp);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 540);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(425, 168);
            this.panel5.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(327, 93);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 17);
            this.label14.TabIndex = 16;
            this.label14.Text = "Thêm";
            // 
            // tbTheloai
            // 
            this.tbTheloai.FormattingEnabled = true;
            this.tbTheloai.Items.AddRange(new object[] {
            "Phim hài",
            "Phim tình cảm",
            "Phim trẻ em",
            "Phim Ngôn tình",
            "Phim Tàu khựa",
            "Phim Thời sự",
            "Phim Ngoại quốc"});
            this.tbTheloai.Location = new System.Drawing.Point(21, 82);
            this.tbTheloai.Name = "tbTheloai";
            this.tbTheloai.Size = new System.Drawing.Size(121, 24);
            this.tbTheloai.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(167, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "File:*";
            // 
            // btPick
            // 
            this.btPick.BackgroundImage = global::Media_TH2.Properties.Resources.pick;
            this.btPick.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPick.Location = new System.Drawing.Point(261, 78);
            this.btPick.Name = "btPick";
            this.btPick.Size = new System.Drawing.Size(32, 32);
            this.btPick.TabIndex = 13;
            this.btPick.UseVisualStyleBackColor = true;
            this.btPick.Click += new System.EventHandler(this.btPick_Click_1);
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(167, 83);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(88, 22);
            this.tbPath.TabIndex = 12;
            // 
            // btThem
            // 
            this.btThem.BackgroundImage = global::Media_TH2.Properties.Resources.add_96px;
            this.btThem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btThem.Location = new System.Drawing.Point(321, 35);
            this.btThem.Name = "btThem";
            this.btThem.Size = new System.Drawing.Size(63, 51);
            this.btThem.TabIndex = 11;
            this.btThem.UseVisualStyleBackColor = true;
            this.btThem.Click += new System.EventHandler(this.btThem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(159, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Đạo diễn:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Diễn viên:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Thể loại";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tên phim:*";
            // 
            // tbDaodien
            // 
            this.tbDaodien.Location = new System.Drawing.Point(161, 37);
            this.tbDaodien.Name = "tbDaodien";
            this.tbDaodien.Size = new System.Drawing.Size(132, 22);
            this.tbDaodien.TabIndex = 3;
            // 
            // tbDienvien
            // 
            this.tbDienvien.Location = new System.Drawing.Point(18, 127);
            this.tbDienvien.Name = "tbDienvien";
            this.tbDienvien.Size = new System.Drawing.Size(123, 22);
            this.tbDienvien.TabIndex = 2;
            // 
            // tbTenp
            // 
            this.tbTenp.Location = new System.Drawing.Point(18, 37);
            this.tbTenp.Name = "tbTenp";
            this.tbTenp.Size = new System.Drawing.Size(123, 22);
            this.tbTenp.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Ten,
            this.Casi,
            this.Theloai});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 105);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(425, 368);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listView1_ItemSelectionChanged);
            // 
            // Ten
            // 
            this.Ten.Text = "Tên phim";
            this.Ten.Width = 160;
            // 
            // Casi
            // 
            this.Casi.Text = "Đạo diễn";
            this.Casi.Width = 70;
            // 
            // Theloai
            // 
            this.Theloai.Text = "Thể loại";
            this.Theloai.Width = 80;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.btSearch);
            this.panel4.Controls.Add(this.tbSearch);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(425, 105);
            this.panel4.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(279, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "Tìm kiếm";
            // 
            // btSearch
            // 
            this.btSearch.BackgroundImage = global::Media_TH2.Properties.Resources.search_64px;
            this.btSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btSearch.Location = new System.Drawing.Point(284, 34);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(41, 33);
            this.btSearch.TabIndex = 1;
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(27, 39);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(251, 22);
            this.tbSearch.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.pnInfo);
            this.panel7.Controls.Add(this.wdMedia);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(611, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(566, 708);
            this.panel7.TabIndex = 7;
            // 
            // pnInfo
            // 
            this.pnInfo.BackColor = System.Drawing.Color.White;
            this.pnInfo.Controls.Add(this.lbDienvien);
            this.pnInfo.Controls.Add(this.lbTenphim);
            this.pnInfo.Controls.Add(this.pictureBox1);
            this.pnInfo.Controls.Add(this.lbNhacsi);
            this.pnInfo.Controls.Add(this.lbArtist);
            this.pnInfo.Controls.Add(this.lbDv);
            this.pnInfo.Controls.Add(this.lbTenp);
            this.pnInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnInfo.Location = new System.Drawing.Point(0, 540);
            this.pnInfo.Name = "pnInfo";
            this.pnInfo.Size = new System.Drawing.Size(566, 168);
            this.pnInfo.TabIndex = 5;
            // 
            // lbNhacsi
            // 
            this.lbNhacsi.AutoSize = true;
            this.lbNhacsi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhacsi.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbNhacsi.Location = new System.Drawing.Point(13, 216);
            this.lbNhacsi.Name = "lbNhacsi";
            this.lbNhacsi.Size = new System.Drawing.Size(0, 25);
            this.lbNhacsi.TabIndex = 5;
            // 
            // lbArtist
            // 
            this.lbArtist.AutoSize = true;
            this.lbArtist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbArtist.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbArtist.Location = new System.Drawing.Point(13, 150);
            this.lbArtist.Name = "lbArtist";
            this.lbArtist.Size = new System.Drawing.Size(0, 25);
            this.lbArtist.TabIndex = 3;
            // 
            // lbDv
            // 
            this.lbDv.AutoSize = true;
            this.lbDv.Location = new System.Drawing.Point(15, 93);
            this.lbDv.Name = "lbDv";
            this.lbDv.Size = new System.Drawing.Size(71, 17);
            this.lbDv.TabIndex = 2;
            this.lbDv.Text = "Diễn viên:";
            // 
            // lbTenp
            // 
            this.lbTenp.AutoSize = true;
            this.lbTenp.Location = new System.Drawing.Point(13, 32);
            this.lbTenp.Name = "lbTenp";
            this.lbTenp.Size = new System.Drawing.Size(71, 17);
            this.lbTenp.TabIndex = 0;
            this.lbTenp.Text = "Tên phim:";
            // 
            // wdMedia
            // 
            this.wdMedia.Dock = System.Windows.Forms.DockStyle.Top;
            this.wdMedia.Enabled = true;
            this.wdMedia.Location = new System.Drawing.Point(0, 0);
            this.wdMedia.Name = "wdMedia";
            this.wdMedia.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wdMedia.OcxState")));
            this.wdMedia.Size = new System.Drawing.Size(566, 537);
            this.wdMedia.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(286, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(277, 162);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // lbTenphim
            // 
            this.lbTenphim.AutoSize = true;
            this.lbTenphim.Location = new System.Drawing.Point(16, 61);
            this.lbTenphim.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbTenphim.Name = "lbTenphim";
            this.lbTenphim.Size = new System.Drawing.Size(0, 17);
            this.lbTenphim.TabIndex = 7;
            // 
            // lbDienvien
            // 
            this.lbDienvien.AutoSize = true;
            this.lbDienvien.Location = new System.Drawing.Point(16, 127);
            this.lbDienvien.Name = "lbDienvien";
            this.lbDienvien.Size = new System.Drawing.Size(0, 17);
            this.lbDienvien.TabIndex = 8;
            // 
            // Movie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.ClientSize = new System.Drawing.Size(1177, 708);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Movie";
            this.Text = "Movie";
            this.Load += new System.EventHandler(this.Movie_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.pnInfo.ResumeLayout(false);
            this.pnInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wdMedia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btInfo;
        private System.Windows.Forms.Button btRecent;
        private System.Windows.Forms.Button btDanhsachtatca;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btEdit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btXoa;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox tbTheloai;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btPick;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Button btThem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDaodien;
        private System.Windows.Forms.TextBox tbDienvien;
        private System.Windows.Forms.TextBox tbTenp;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Ten;
        private System.Windows.Forms.ColumnHeader Casi;
        private System.Windows.Forms.ColumnHeader Theloai;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel pnInfo;
        private System.Windows.Forms.Label lbNhacsi;
        private System.Windows.Forms.Label lbArtist;
        private System.Windows.Forms.Label lbDv;
        private System.Windows.Forms.Label lbTenp;
        private AxWMPLib.AxWindowsMediaPlayer wdMedia;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbDienvien;
        private System.Windows.Forms.Label lbTenphim;
    }
}